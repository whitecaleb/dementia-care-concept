﻿namespace DementiaApp.Domain
{
	using System;

	public class Pill
	{
		public string Name { get; set; }

		public int Milligrams { get; set; }

		public int Time { get; set; }

		private string getDetail()
		{
			if (Time > 12) {
				return (Time-12) + "pm " + Milligrams + "mg";
			} else {
				return Time + "am " + Milligrams + "mg";
			}
		}
	}
}