﻿namespace DementiaPatientApp
{
	using System;
	using Newtonsoft.Json;

	public class PillTakenLog
	{
		public string Id { get; set; }

		[JsonProperty(PropertyName = "text")]
		public string Text { get; set; }

		[JsonProperty(PropertyName = "date")]
		public string TakenAtDate { get; set; }

		[JsonProperty(PropertyName = "time")]
		public string TakenAtTime { get; set; }
	}

	public class PillTakenLogWrapper : Java.Lang.Object
	{
		public PillTakenLog PillTakenLog { get; private set; }

		public PillTakenLogWrapper(PillTakenLog item)
		{
			this.PillTakenLog = item;
		}
	}
}