﻿namespace DementiaPatientApp
{
	using System;
	using System.Collections.Generic;
	using Android.Gms.Wearable;

	public class PillReminder : Java.Lang.Object
	{
		private IList<DataMap> pillDMs;
		private static int _reminderId = 0;

		public PillReminder(IList<DataMap> pillDataMaps, int reminderId)
		{
			pillDMs = pillDataMaps;
			_reminderId=reminderId;
		}

		public PutDataRequest ToPutDataRequest()
		{
			var request = PutDataMapRequest.Create("/pillreminder/" + _reminderId);
			var dataMap = request.DataMap;
			dataMap.PutDataMapArrayList ("pill_DataMaps", pillDMs);
			dataMap.PutInt ("reminderId", _reminderId);
			return request.AsPutDataRequest();
		}
	}
}