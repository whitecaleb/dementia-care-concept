﻿namespace DementiaPatientApp
{
	using System;
	using Newtonsoft.Json;

	public class Pill
	{
		public string Id { get; set; }

		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "milligrams")]
		public int Milligrams { get; set; }

		[JsonProperty(PropertyName = "time")]
		public int Time { get; set; }
	}
}