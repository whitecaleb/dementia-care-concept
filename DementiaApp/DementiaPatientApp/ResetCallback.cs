﻿using System;
using Android.Gms.Common.Apis;
using Android.Gms.Wearable;
using Android.Gms.Common.Data;
using Android.Util;
using System.Collections.Generic;
using Java.Interop;

namespace DementiaPatientApp
{
	public class ResetCallback : Java.Lang.Object,IResultCallback
	{
		MainActivity act;

		public ResetCallback (MainActivity activity)
		{
			act = activity;
		}

		public void OnResult (Java.Lang.Object obj)
		{
			if (obj is DataItemBuffer) {
				var result = Android.Runtime.Extensions.JavaCast<DataItemBuffer> (obj);
				if (result.Status.IsSuccess) {
					//var dataItemList = FreezableUtils.FreezeIterable (result);

					var dataItemUriList = new List<Android.Net.Uri> ();
					//TODO clean loop up, can't foreach the loop easily...
					var iterator = result.Iterator ();
					while (iterator.HasNext) {
						var current = iterator.Next ().JavaCast<IDataItem> ();
						dataItemUriList.Add (current.Uri);
					}

					result.Close ();
					//act.ResetDataItems (dataItemList);
					act.DeleteDataItems (dataItemUriList);
				} else {
					if (Log.IsLoggable (MainActivity.Tag, LogPriority.Debug))
						Log.Debug (MainActivity.Tag, "Reset reminder: failed to get Data Items to reset");
				}
				result.Close ();
			}
		}

		public void OnResult (DataItemBuffer result)
		{
			if (result.Status.IsSuccess) {
				//var dataItemList = FreezableUtils.FreezeIterable (result);
				List<Android.Net.Uri> dataItemUriList = new List<Android.Net.Uri> ();
				var iterator = result.Iterator ();
				while (iterator.HasNext) {
					var dataItem = iterator.Next ().JavaCast<IDataItem> ();
					dataItemUriList.Add (dataItem.Uri);
				}
				result.Close ();
				//act.ResetDataItems (dataItemList);
				act.DeleteDataItems (dataItemUriList);
			} else {

				if (Log.IsLoggable (MainActivity.Tag, LogPriority.Debug))
					Log.Debug (MainActivity.Tag, "Reset reminder: failed to get Data Items to reset");
			}
			result.Close ();
		}
	}
}

