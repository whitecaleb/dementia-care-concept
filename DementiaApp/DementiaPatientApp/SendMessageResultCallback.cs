﻿namespace DementiaPatientApp
{
	using System;
	using Android.Gms.Common.Apis;
	using Android.Gms.Wearable;

	public class SendMessageResultCallback : Java.Lang.Object,IResultCallback
	{
		MainActivity act;
		string path;
		byte[] data;

		public SendMessageResultCallback (MainActivity activity, string path, byte[] data)
		{
			act = activity;
			this.path = path;
			this.data = data;
		}

		public void OnResult (Java.Lang.Object obj)
		{
			var nodes = Android.Runtime.Extensions.JavaCast<INodeApiGetConnectedNodesResult> (obj);
			foreach (INode node in nodes.Nodes) {
				WearableClass.MessageApi.SendMessage (act._googleApiClient, node.Id, path, data);
			}
			if (path.Equals ("/exited") && act._googleApiClient.IsConnected)
				act._googleApiClient.Disconnect ();
		}
		/*
		public void OnResult(INodeApiGetConnectedNodesResult nodes)
		{
			foreach (INode node in nodes.Nodes) {
				WearableClass.MessageApi.SendMessage (act._googleApiClient, node.Id, act.path, act.data);
			}

			if (act.path.Equals (Constants.QUIZ_EXITED_PATH) && act._googleApiClient.IsConnected) {
				act.google_api_client.Disconnect ();
			}
		}
		*/
	}
}

