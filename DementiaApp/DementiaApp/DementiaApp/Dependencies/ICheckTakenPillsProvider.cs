namespace DementiaApp.Dependencies
{
	using System.Collections.Generic;
	using System.Threading.Tasks;

	public interface ICheckTakenPillsProvider
	{
		Task<string[]> GetTakenPills();
		Task<List<Domain.Pill>> GetPatientPills();
		void InitialiseMobileServices ();
	}
}