﻿namespace DementiaApp.ViewModels
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Runtime.CompilerServices;
	using System.Threading.Tasks;

	using TinyIoC;

	using Xamarin.Forms;

	public abstract class BaseViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public abstract string Title { get; }

		protected virtual T PushPageModel<T>(object data = null, bool modal = false)
			where T : BaseViewModel
		{
			return (T)this.PushPageModel(typeof(T), data, modal);
		}

		protected virtual BaseViewModel PushPageModel(Type viewmodelType, object data = null, bool modal = false)
		{
			var viewModel = (BaseViewModel)TinyIoC.TinyIoCContainer.Current.Resolve(viewmodelType);
			var page = ResolveViewModel(viewmodelType, data, viewModel);
			var rootNav = TinyIoC.TinyIoCContainer.Current.Resolve<IRootNavigation>();

			if (modal)
			{
				var navPage = new NavigationPage(page)
				{
					Title = page.Title
				};
				var closeToolbarItem = new ToolbarItem()
				{
					Text = "Close"
				};

				closeToolbarItem.Clicked += (sender, e) =>
				{
					this.ModalClosed();
					this.PopPageModel(modal: true);
				};

				navPage.ToolbarItems.Add(closeToolbarItem);
				rootNav.PushModalPage(navPage);
			}
			else
			{
				rootNav.PushPage(page);
				rootNav.SelectPageInNavigation(viewmodelType);
			}

			return (BaseViewModel)viewModel;
		}

		public static Page ResolveViewModel<T>()
			where T : BaseViewModel
		{
			var viewModel = TinyIoC.TinyIoCContainer.Current.Resolve<T>();

			return ResolveViewModel(typeof(T), null, viewModel);
		}

		public static Page ResolveViewModel(Type viewModelType, object data, BaseViewModel viewModel)
		{
			var @namespace = viewModelType.Namespace.Replace(".ViewModels", ".Views");
			var name = viewModelType.Name.Replace("ViewModel", string.Empty);
			var fullName = @namespace + "." + name;
			var pageType = Type.GetType(fullName);

			if (pageType == null)
			{
				pageType = Type.GetType(fullName);
			}

			var page = (Page)TinyIoC.TinyIoCContainer.Current.Resolve(pageType);

			page.BindingContext = viewModel;

			var initMethod = TinyIoC.TypeExtensions.GetMethod(viewModelType, "Init");

			if (initMethod != null)
			{
				if (initMethod.GetParameters().Length > 0)
				{
					initMethod.Invoke(viewModel, new object[] { data });
				}
				else
				{
					initMethod.Invoke(viewModel, null);
				}
			}

			var vmProperty = TinyIoC.TypeExtensions.GetProperty(pageType, "ViewModel");

			if (vmProperty != null)
			{
				vmProperty.SetValue(page, viewModel);
			}

			var vmPageBindingContext = TinyIoC.TypeExtensions.GetProperty(pageType, "BindingContext");

			if (vmPageBindingContext != null)
			{
				vmPageBindingContext.SetValue(page, viewModel);
			}

			NavigationPage.SetHasBackButton(page, false);

			return page;
		}

		public virtual void Init(object data)
		{
		}

		public virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		protected virtual void ModalClosed()
		{
		}

		protected void PopPageModel(bool modal = false)
		{
			var rootNav = TinyIoC.TinyIoCContainer.Current.Resolve<IRootNavigation>();

			if (modal)
			{
				rootNav.PopModalPage();
			}
			else
			{
				rootNav.PopPage();
			}
		}

		protected void ShowAlert(string title, string message, string buttonText)
		{
			var rootNav = TinyIoC.TinyIoCContainer.Current.Resolve<IRootNavigation>();

			rootNav.ShowAlert(title, message, buttonText);
		}

		protected async Task<bool> ShowConfirm(string title, string message, string okButtonText, string cancelButtonText)
		{
			var rootNav = TinyIoC.TinyIoCContainer.Current.Resolve<IRootNavigation>();

			return await rootNav.ShowConfirm(title, message, okButtonText, cancelButtonText);
		}
	}
}