﻿namespace DementiaApp
{
	using System;
	using System.Threading.Tasks;

	using DementiaApp.ViewModels;

	using Xamarin.Forms;

	public interface IRootNavigation
	{
		event EventHandler<bool> MenuStateChanged;

		void PushPage(Page page);

		void PushModalPage(Page page);

		void PopPage();

		void PopModalPage();

		void ShowAlert(string title, string message, string buttonText);

		bool IsMenuOpen();

		void ShowMenu();

		void HideMenu();

		void SwitchMenuVisibility();

		void SelectPageInNavigation(Type viewmodelType);

		Task<bool> ShowConfirm(string title, string message, string okButtonText, string cancelButtonText);
	}
}