﻿namespace DementiaApp.Views
{
	using System;
	using System.Threading.Tasks;
	using DementiaApp.ViewModels;

	using TinyIoC;

	using Xamarin.Forms;

	public class RootContainer : MasterDetailPage, IRootNavigation
	{
		public event EventHandler<bool> MenuStateChanged;

		public RootContainer()
		{
			//this.leftHandMenu = new LeftHandMenu();

			var landingPage = this.GetLandingPage();
			var menu = new ContentPage()
			{
				Title = "Menu"
			};

			//menu.Content = leftHandMenu;

			this.Master = new NavigationPage(menu)
			{
				Title = "Menu",
				BarTextColor = Color.White
			};

			this.Detail = new NavigationPage(landingPage)
			{
				Title = "Welcome"
			};

			this.IsGestureEnabled = false;

			NavigationPage.SetHasBackButton(this.Detail, false);

			this.IsPresentedChanged += (sender, e) =>
			{
				if (this.MenuStateChanged != null)
				{
					this.MenuStateChanged.Invoke(this, this.IsPresented);
				}
			};
		}

		public void PushPage(Page page)
		{
			this.HideMenu();

			bool animated = Device.OS == TargetPlatform.iOS;

			((NavigationPage)Detail).PushAsync(page, animated);
		}

		public async void PushModalPage(Page page)
		{
			await Navigation.PushModalAsync(page, true);
		}

		public async void PopModalPage()
		{
			await Navigation.PopModalAsync();
		}

		public void PopPage()
		{
			((NavigationPage)Detail).PopAsync();
		}

		public void ShowAlert(string title, string message, string buttonText)
		{
			((NavigationPage)Detail).DisplayAlert(title, message, buttonText);
		}

		public async Task<bool> ShowConfirm(string title, string message, string okButtonText, string cancelButtonText)
		{
			return await ((NavigationPage)Detail).DisplayAlert(title, message, okButtonText, cancelButtonText);
		}

		public bool IsMenuOpen()
		{
			return this.IsPresented;
		}

		public void ShowMenu()
		{
			this.IsPresented = true;
		}

		public void HideMenu()
		{
			this.IsPresented = false;
		}

		public void SwitchMenuVisibility()
		{
			this.IsPresented = !this.IsPresented;
		}

		public void SelectPageInNavigation(Type viewmodelType)
		{
			//this.leftHandMenu.SelectPage(viewmodelType);
		}

		private Page GetLandingPage()
		{
			// return BaseViewModel.ResolveViewModel<CreateReportViewModel>();

			return BaseViewModel.ResolveViewModel<HomeViewModel>();
		}
	}
}