﻿/**
 * Icon created by Ashley van Dyck from Noun Project
 */
namespace DementiaApp
{
	using DementiaApp.Views;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using Xamarin.Forms;

	public class App : Application
	{
		public string Medicine;

		public App ()
		{
			// The root page of your application
			MainPage = new RootContainer();
		}

		protected override void OnStart ()
		{
			base.OnStart();
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

		[Xamarin.Forms.ContentProperty("Content")]
		public class ContentPage : Page
		{

		}
	}
}