namespace DementiaApp.Droid.Dependencies
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;

	using Android.App;
	using Android.Content;
	using Android.OS;
	using Android.Runtime;
	using Android.Views;
	using Android.Widget;
	using Android.Gms.Common.Apis;
	using Android.Gms.Wearable;
	using Android.Util;
	using Xamarin.Forms;
	using DementiaApp.Dependencies;
	using Microsoft.WindowsAzure.MobileServices;

	public class CheckTakenPillsProvider : ICheckTakenPillsProvider
	{
		public static MobileServiceClient client = new MobileServiceClient(
			"https://dimentia-care-concept.azure-mobile.net/", "GMKEVTLNlJPgIDkjIhkemsaFrjuVrb90");

		//private Dictionary<string, Domain.Pill> patPills = new Dictionary<string, Domain.Pill>();
		private List<Domain.Pill> patientPills = new List<Domain.Pill>();
		private List<string> takenPills = new List<string> ();

		public async Task<string[]> GetTakenPills()
		{
			await CheckTakenPills ();

			return takenPills.ToArray();
		}

		public async Task<List<Domain.Pill>> GetPatientPills()
		{
			await CheckPatientPills();

			return patientPills;
		}

		/**
		 * Check Azure for pills taken today, put them in the takenPills list.
		 */
		private async Task CheckTakenPills()
		{
			var dateToday = DateTime.Now.Date.ToString ("yyyy-MM-dd");
			takenPills.Clear ();

			// select the text from the azure PillTakenLog table where the pill was taken today
			List<string> names = await client.GetTable<PillTakenLog> ()
				.Where(item => item.TakenAtDate == dateToday)
				.Select(item => item.Text)
				.ToListAsync();

			takenPills = names;
		}

		private async Task CheckPatientPills()
		{
			patientPills.Clear ();

			// select the name from the azure Pills table
			List<string> names = await client.GetTable<Pill>()
				.Select(item => item.Name)
				.ToListAsync();
			List<int> times = await client.GetTable<Pill>()
				.Select(item => item.Time)
				.ToListAsync();
			List<int> mgs = await client.GetTable<Pill>()
				.Select(item => item.Milligrams)
				.ToListAsync();

			for (int i = 0; i < names.Count; i++)
			{
				patientPills.Add(new Domain.Pill() { Name=names[i], Milligrams=mgs[i], Time=times[i] });
			}
		}

		public void InitialiseMobileServices()
		{
			CurrentPlatform.Init();
		}
	}
}