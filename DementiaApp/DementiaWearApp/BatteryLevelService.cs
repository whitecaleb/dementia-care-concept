﻿namespace DementiaWearApp
{
	using Android.App;
	using Android.Content;
	using Android.OS;
	using Android.Util;

	using Android.Gms.Common;
	using Android.Gms.Common.Apis;
	using Android.Gms.Wearable;

	using Java.Util.Concurrent;
	using Java.Interop;

	[Service]
	public class BatteryLevelService : IntentService,IGoogleApiClientConnectionCallbacks,
	IGoogleApiClientOnConnectionFailedListener
	{
		const string Tag = "BatteryLevelReciever";

		private IGoogleApiClient _googleApiClient;

		public override void OnCreate ()
		{
			base.OnCreate ();
			_googleApiClient = new GoogleApiClientBuilder (this)
				.AddApi (WearableClass.API)
				.AddConnectionCallbacks (this)
				.AddOnConnectionFailedListener (this)
				.Build ();
		}

		protected override void OnHandleIntent (Intent intent)
		{
			_googleApiClient.BlockingConnect (100, TimeUnit.Milliseconds);
			Android.Net.Uri dataItemUri = intent.Data;
			if (!_googleApiClient.IsConnected) {
				Log.Error (Tag, "Failed to update data item " + dataItemUri 
					+ " because client is disconnected from Google Play Services");
				return;
			}
			var dataItemResult = WearableClass.DataApi.GetDataItem (
				_googleApiClient, dataItemUri).Await ().JavaCast<IDataApiDataItemResult>();
			var putDataMapRequest = PutDataMapRequest
				.CreateFromDataMapItem (DataMapItem.FromDataItem (dataItemResult.DataItem));
			var dataMap = putDataMapRequest.DataMap;
			var battery = intent.GetDoubleExtra ("battery", 0);
			dataMap.PutDouble ("watch_battery", battery);
			dataMap.PutBoolean ("notification_was_deleted", true);
			var request = putDataMapRequest.AsPutDataRequest ();
			WearableClass.DataApi.PutDataItem (_googleApiClient, request).Await ();
			_googleApiClient.Disconnect ();
		}

		public void OnConnected(Bundle bundle)
		{
		}

		public void OnConnectionSuspended(int i)
		{
		}

		public void OnConnectionFailed(ConnectionResult connectionResult)
		{
		}
	}
}

