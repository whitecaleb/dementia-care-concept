namespace DementiaWearApp
{
	using System;
	using System.Globalization;

	using Android.App;
	using Android.Content;
	using Android.OS;
	using Android.Util;
	using Android.Gms.Common;
	using Android.Gms.Common.Apis;
	using Android.Gms.Wearable;

	using Java.Util.Concurrent;
	using Java.Interop;

	[Service]
	public class UpdatePillReminderService : IntentService, IGoogleApiClientConnectionCallbacks,
	IGoogleApiClientOnConnectionFailedListener
	{
		const int TimeOutMs = 100;
		const string Tag = "UpdatePillReminderService";

		IGoogleApiClient _googleApiClient;

		public UpdatePillReminderService() : base()
		{
		}

		public override void OnCreate()
		{
			base.OnCreate();

			Log.Info(Tag, "OnCreate");

			_googleApiClient = new GoogleApiClientBuilder(this)
				.AddApi(WearableClass.API)
				.AddConnectionCallbacks(this)
				.AddOnConnectionFailedListener(this)
				.Build();
		}

		protected override void OnHandleIntent(Intent intent)
		{
			_googleApiClient.BlockingConnect(TimeOutMs, TimeUnit.Milliseconds);
			Android.Net.Uri dataItemUri = intent.Data;
			if (!_googleApiClient.IsConnected) {
				Log.Error(Tag, "Failed to update data item " + dataItemUri +
					" because client is disconnected from Google Play Services");
				return;
			}
			var dataItemResult = WearableClass.DataApi.GetDataItem(
				_googleApiClient, dataItemUri).Await().JavaCast<IDataApiDataItemResult>();
			var putDataMapRequest = PutDataMapRequest
				.CreateFromDataMapItem(DataMapItem.FromDataItem(dataItemResult.DataItem));

			var dataMap = putDataMapRequest.DataMap;

			//update pill status variables
			var time = DateTime.Now.ToString (CultureInfo.InvariantCulture);
			var pillName = intent.GetStringExtra ("pillName");
			var index = intent.GetIntExtra ("index", 0);
			var battery = intent.GetDoubleExtra ("battery", 0);

			dataMap.PutString("taken_pill_name", pillName);
			dataMap.PutString("time", time);
			dataMap.PutInt ("response_type", 0);
			dataMap.PutDouble ("watch_battery", battery);

			PutDataRequest request = putDataMapRequest.AsPutDataRequest();
			WearableClass.DataApi.PutDataItem (_googleApiClient, request).Await();

			((NotificationManager)GetSystemService (NotificationService)).Cancel (0);
			_googleApiClient.Disconnect();
		}

		public void OnConnected(Bundle connectionHint)
		{
		}

		public void OnConnectionSuspended(int cause)
		{
		}

		public void OnConnectionFailed(ConnectionResult result)
		{
		}
	}
}