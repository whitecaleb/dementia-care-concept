namespace DementiaWearApp
{
    using System;
    using Android.Gms.Wearable;
    using Android.Gms.Common.Apis;
    using Android.Runtime;
    using Android.Util;
    using Android.App;
    using Android.Content;
    using Android.Support.V4.App;

    [Service(), IntentFilter(new string[] { "com.google.android.gms.wearable.BIND_LISTENER" }),
        IntentFilter(new string[] { "com.example.android.wearable.synchronizednotifications.DISMISS" })]
    class NotificationUpdateService : WearableListenerService, IGoogleApiClientConnectionCallbacks,
    IGoogleApiClientOnConnectionFailedListener, IResultCallback
    {
        public const string MessagePath = "/message_path";
        private const string Tag = "NotificationUpdate";
        private IGoogleApiClient _googleApiClient;

        public override void OnCreate()
        {
            base.OnCreate();
            _googleApiClient = new GoogleApiClientBuilder(this)
                    .AddApi(WearableClass.Api)
                    .AddConnectionCallbacks(this)
                    .AddOnConnectionFailedListener(this)
                    .Build();
        }

        public override Android.App.StartCommandResult OnStartCommand(Android.Content.Intent intent, Android.App.StartCommandFlags flags, int startId)
        {
            //if (intent != null)
            //{
            //    string action = intent.Action;
            //    if (Constants.ActionDismiss.Equals(action))
            //    {
            //        // We need to dismiss the wearable notification. We delete the DataItem that created the notification to inform the wearable.
            //        int notificationId = intent.GetIntExtra("notification-id", -1);
            //        if (notificationId == 4)
            //        {
            //            DismissWearableNotification(notificationId);
            //        }
            //    }
            //}
            return base.OnStartCommand(intent, flags, startId);
        }

        public void OnDataChanged(DataEventBuffer dataEvents)
        {
            for (int i = 0; i < dataEvents.Count; i++)
            {
                var dataEvent = dataEvents.Get(i).JavaCast<IDataEvent>();
                if (dataEvent.Type == DataEvent.TypeChanged)
                {
                    DataMap dataMap = DataMapItem.FromDataItem(dataEvent.DataItem).DataMap;
                    var content = dataMap.GetString("content");
                    var title = dataMap.GetString("title");
                    if (MessagePath.Equals(dataEvent.DataItem.Uri.Path))
                        ShowNotification(title, content);
                }
                else if (dataEvent.Type == DataEvent.TypeDeleted)
                {
                    if (Log.IsLoggable(Tag, LogPriority.Debug))
                        Log.Debug(Tag, "DataItem deleted: " + dataEvent.DataItem.Uri.Path);
                    if (MessagePath.Equals(dataEvent.DataItem.Uri.Path))
                        ((NotificationManager)GetSystemService(NotificationService)).Cancel(4);
                }
            }
        }

        public void ShowNotification(string title, string content)
        {
            int notificationId = 001;

            // set intent to open the app
            Intent startIntent = new Intent(this, typeof(MainActivity)).SetAction(Intent.ActionMain);
            PendingIntent startPendingIntent =
                PendingIntent.GetActivity(this, 0, startIntent, PendingIntentFlags.CancelCurrent);

            // build notification
            Notification notif = new Notification.Builder(this)
                .SetContentTitle(title)
                .SetContentText(content)
                .SetSmallIcon(Resource.Drawable.Icon24)
                .SetLocalOnly(true)
                .SetAutoCancel(true)
                .AddAction(Resource.Drawable.Icon24, "Blah", startPendingIntent)
                .Extend(new Notification.WearableExtender()
                    .SetHintHideIcon(true)
                    .SetContentAction(0))
                .Build();

            // replace AddAction() and Extend() with this for swipe left to open
            //.SetContentIntent(startPendingIntent)

            // display notification
            NotificationManagerCompat notificationManager = NotificationManagerCompat.From(this);
            notificationManager.Notify(notificationId, notif);
        }

        public void OnConnected(Android.OS.Bundle connectionHint)
        {
            var dataItemUri = new Android.Net.Uri.Builder().Scheme(PutDataRequest.WearUriScheme).Path(MessagePath).Build();
            if (Log.IsLoggable(Tag, LogPriority.Debug))
            {
                Log.Debug(Tag, "Deleting Uri: " + dataItemUri.ToString());
            }
            WearableClass.DataApi.DeleteDataItems(_googleApiClient, dataItemUri).SetResultCallback(this);
        }

        public void OnConnectionSuspended(int cause) { }

        public void OnConnectionFailed(Android.Gms.Common.ConnectionResult result) { }

        public void OnResult(Java.Lang.Object result)
        {
            _googleApiClient.Disconnect();
            IDataApiDeleteDataItemsResult deleteDataItemsResult;
            try
            {
                deleteDataItemsResult = result.JavaCast<IDataApiDeleteDataItemsResult>();
            }
            catch
            {
                return;
            }
            if (!deleteDataItemsResult.Status.IsSuccess)
                Log.Error(Tag, "DismissWearableNotification(): Failed to delete IDataItem");
        }
    }
}