﻿namespace DementiaWearApp
{
	using Android.Graphics;

	public class PillNotification
	{
		private static int _index = 0;
		private readonly int _incIndex = _index++;

		public string PillName { get; set; }

		public Bitmap PillPicture { get; set; }

		public int PillIndex { get { return _incIndex; } }
	}
}