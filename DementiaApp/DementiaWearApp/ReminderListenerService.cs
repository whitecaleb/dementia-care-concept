using System.Threading.Tasks;

namespace DementiaWearApp
{
	using System;
	using System.IO;
	using System.Collections.Generic;
	using Android.App;
	using Android.Content;
	using Android.Net;
	using Android.Text;
	using Android.Text.Style;
	using Android.Util;
	using Android.Views;
	using Android.Graphics;
	using Android.Graphics.Drawables;

	using Android.Support.Wearable.Activity;
	using Android.Support.V4.App;
	using Android.OS;

	using Android.Gms.Common;
	using Android.Gms.Common.Apis;
	using Android.Gms.Common.Data;
	using Android.Gms.Wearable;

	using Java.Util;
	using Java.Util.Concurrent;
	using Java.Interop;
	using System.Globalization;

	[Service]
	[IntentFilter(new string[] { "com.google.android.gms.wearable.BIND_LISTENER" })]
	public class ReminderListenerService : WearableListenerService
	{
		const string Tag = "ReminderListener";

		private static Dictionary<string, Bitmap> pillImages = new Dictionary<string, Bitmap> ();

		public ReminderListenerService ()
		{

		}

		public override void OnCreate ()
		{
			base.OnCreate ();
		}

		public override void OnDataChanged(DataEventBuffer eventBuffer)
		{
			var events = FreezableUtils.FreezeIterable (eventBuffer);
			eventBuffer.Close ();

			var googleApiClient = new GoogleApiClientBuilder (this)
				.AddApi (WearableClass.API)
				.Build ();

			var connectionResult = googleApiClient.BlockingConnect (100, TimeUnit.Milliseconds);

			if (!connectionResult.IsSuccess) {
				Log.Error ("PillReminder", "ReminderListenerService failed to connect to GoogleApiClient.");
				return;
			}
			try {
				foreach (var ev in events)
				{
					var e = ((Java.Lang.Object)ev).JavaCast<IDataEvent> ();
					if (e.Type == DataEvent.TypeChanged) {
							
						Log.Info(Tag, "TypeChanged");

						var dataItem = e.DataItem;
						var dataMap = DataMapItem.FromDataItem (dataItem).DataMap;

						// Load data from phone into variables
						var pillDataMaps = dataMap.GetDataMapArrayList ("pill_DataMaps");


						BuildNotification (pillDataMaps, dataItem.Uri);

					} else if (e.Type == DataEvent.TypeDeleted) {

						Log.Info(Tag, "TypeDeleted");

						Android.Net.Uri uri = e.DataItem.Uri;
						//URIs are in the form of "/pill_reminder/0", "/pill_reminder/1" etc.
						//We use the question index as the notification id.
						var notificationId = Java.Lang.Integer.ParseInt (uri.LastPathSegment);
						((NotificationManager)GetSystemService (NotificationService)).Cancel (0);
					}
					// Delete message notification.
					((NotificationManager)GetSystemService (NotificationService)).Cancel (-1);
				}
			} catch (Exception ex) {
				Log.Error (Tag, ex.Message);
			}
			googleApiClient.Disconnect();
		}

		public override void OnMessageReceived (IMessageEvent messageEvent)
		{
			Log.Info(Tag, "MessageReceived");

			string path = messageEvent.Path;
			if (path.Equals ("/exited")) {
				((NotificationManager)GetSystemService (NotificationService)).CancelAll ();
				//ExitedNotification (messageEvent);
			}
			if (path.Equals ("/deleted")) {
				DeletedNotification (messageEvent);
			}
			if (path.Equals ("/battery_warning")) {
				BattWarnNotification (messageEvent);
			}
			if (path.Equals ("/delay_reminder")) {
				DelayReminderNotification (messageEvent);
			}
			if (path.Equals ("/no_pills")) {
				NoPillNotification (messageEvent);
			}
		}

		/**
         * Notification with a page for each pill in the DataMap array.
         * 
         * @ param pillDataMaps list of the pill notification pages as DataMaps
         * @ param uri Data for the pill intent
         */
		private void BuildNotification (IList<DataMap> pillDataMaps, Android.Net.Uri uri)
		{
			Log.Info(Tag, "begin notification build");

			// Create delete intent.
			Intent deleteOperation = new Intent (this, typeof(DeleteNotificationService));
			deleteOperation.SetData (uri);
			deleteOperation.PutExtra ("battery", GetBatteryLevel ());
			PendingIntent deleteIntent = PendingIntent.GetService (this, 0,
				deleteOperation, PendingIntentFlags.UpdateCurrent);

			// First page of the notification.
			var builder = new Notification.Builder (this)
				.SetContentTitle (GetString (Resource.String.welcome_title))
				.SetContentText (GetString (Resource.String.welcome_text))
				.SetSmallIcon (Resource.Drawable.Icon24)
				.SetDefaults (NotificationDefaults.All)
				.SetLocalOnly (true)
				.SetDeleteIntent (deleteIntent);

			// add pills as actions
			var wearableOptions = new Notification.WearableExtender();

			for (var i = 0; i < pillDataMaps.Count; i++)
			{
				Log.Info(Tag, "begin build page"+i);

				// Get data from the DataMap
				var pillName = pillDataMaps [i].GetString ("name");
                /*var pillPic = pillDataMaps[i].GetByteArray("pic");
                // Decode byte[] as bitmap.
				var pic = BitmapFactory.DecodeByteArray(pillPic, 0, pillPic.Length);*/


                // Build notification page
                Notification pillPage = new Notification.Builder (this)
					.SetContentTitle (pillName)
					.SetContentText ("Touch if taken.")
					//.SetLargeIcon (GetLargeIcon(pillDataMaps [i]))
					//.SetLargeIcon (GetLargeIcon(pillName))
					.Extend (new Notification.WearableExtender ().SetContentAction (i))
					.Build ();

				var updateOperation = new Intent (this, typeof(UpdatePillReminderService));
				//Give each intent a unique action.
				updateOperation.SetAction("reminder_" + 0 + "_pill_" + i);
				updateOperation.SetData (uri);
				updateOperation.PutExtra ("index", 0);
				updateOperation.PutExtra ("pillName", pillName);
				updateOperation.PutExtra ("battery", GetBatteryLevel ());

				var updateIntent = PendingIntent.GetService (this, 0, updateOperation, PendingIntentFlags.UpdateCurrent);
				Notification.Action action = new Notification.Action.Builder (Resource.Drawable.ok32, (string)null, updateIntent).Build ();


				wearableOptions.AddAction (action).AddPage (pillPage);
			}
			Log.Info(Tag, "Pages created");

			// Notification extends the pages and is displayed
			builder.Extend(wearableOptions);
			Notification notification = builder.Build();
			((NotificationManager)GetSystemService(NotificationService)).Notify(0, notification);

			Log.Info(Tag, "NotificationBuilt");
		}

		/**
         * Notification when there are no pills sent from the phone.
         */
		private void NoPillNotification(IMessageEvent messageEvent)
		{
			var dataMap = DataMap.FromByteArray (messageEvent.GetData ());
			var message = dataMap.GetString ("message");

			var builder = new Notification.Builder(this)
				.SetContentTitle(GetString(Resource.String.welcome_title))
				.SetContentText("You have no pills to take.")
				.SetSmallIcon(Resource.Drawable.ok24)
				.SetDefaults(NotificationDefaults.All)
				.SetLocalOnly(true);

			// Build and display notification
			Notification notification = builder.Build();
			((NotificationManager)GetSystemService(NotificationService)).Notify(-1, notification);
		}
		private void NoPillNotification()
		{
			var builder = new Notification.Builder(this)
				.SetContentTitle(GetString(Resource.String.welcome_title))
				.SetContentText("You have no pills to take.")
				.SetSmallIcon(Resource.Drawable.ok24)
				.SetDefaults(NotificationDefaults.All)
				.SetLocalOnly(true);

			// Build and display notification
			Notification notification = builder.Build();
			((NotificationManager)GetSystemService(NotificationService)).Notify(-1, notification);
		}

		private void BattWarnNotification(IMessageEvent messageEvent)
		{
			var dataMap = DataMap.FromByteArray (messageEvent.GetData ());
			var exitMsg = dataMap.GetString ("delete_message");

			var builder = new Notification.Builder (this)
				.SetContentTitle ("Warning! Low battery")
				.SetContentText ("Please put the watch on charge.")
				.SetSmallIcon (Resource.Drawable.low_battery)
				.SetDefaults (NotificationDefaults.All)
				.SetLocalOnly (true);

			((NotificationManager)GetSystemService (NotificationService))
				.Notify (-1, builder.Build ());
		}

		private void DelayReminderNotification(IMessageEvent messageEvent)
		{
			var dataMap = DataMap.FromByteArray (messageEvent.GetData ());
			var message = dataMap.GetString ("message");

			var builder = new Notification.Builder (this)
				.SetContentTitle ("Reminder Set!")
				.SetContentText (message)
				.SetSmallIcon (Resource.Drawable.ok24)
				.SetDefaults (NotificationDefaults.All)
				.SetLocalOnly (true);

			((NotificationManager)GetSystemService (NotificationService))
				.Notify (-1, builder.Build ());
		}

		private void ExitedNotification(IMessageEvent messageEvent)
		{
			var dataMap = DataMap.FromByteArray (messageEvent.GetData ());
			var exitMsg = dataMap.GetString ("exit_message");

			//.SetContentTitle (GetString (Resource.String.quiz_report))
			var builder = new Notification.Builder (this)
				.SetSmallIcon (Resource.Drawable.alert)
				.SetDefaults (NotificationDefaults.All)
				.SetContentText (exitMsg)
				.SetLocalOnly (true);
			
			((NotificationManager)GetSystemService (NotificationService))
					.Notify (-1, builder.Build ());
		}

		private void DeletedNotification(IMessageEvent messageEvent)
		{
			var dataMap = DataMap.FromByteArray (messageEvent.GetData ());
			var exitMsg = dataMap.GetString ("delete_message");

			//.SetContentTitle (GetString (Resource.String.quiz_report))
			var builder = new Notification.Builder (this)
				.SetSmallIcon (Resource.Drawable.alert)
				.SetDefaults (NotificationDefaults.All)
				.SetContentText (exitMsg)
				.SetLocalOnly (true);

			((NotificationManager)GetSystemService (NotificationService))
				.Notify (-1, builder.Build ());
		}

		/**
		 * Battery code found at:
		 * http://blog.thomaslebrun.net/2014/02/introduction-to-androidios-development-using-visual-studio-and-xamarin/#.VcSHbSZVikp
		 */
		private double GetBatteryLevel()
		{
			var filter = new IntentFilter (Intent.ActionBatteryChanged);
			var battery = RegisterReceiver (null, filter);
			int level = battery.GetIntExtra (BatteryManager.ExtraLevel, -1);
			int scale = battery.GetIntExtra(BatteryManager.ExtraScale, -1);

			var batteryLevel = Math.Floor(level * 100D / scale);

			int status = battery.GetIntExtra(BatteryManager.ExtraStatus, -1);
			var isCharging = status == (int)BatteryStatus.Charging || status == (int)BatteryStatus.Full;

			// the next three could be useful...
			// How are we charging?
			int chargePlug = battery.GetIntExtra(BatteryManager.ExtraPlugged, -1);
			var usbCharge = chargePlug == (int)BatteryPlugged.Usb;
			var acCharge = chargePlug == (int)BatteryPlugged.Ac;

			return batteryLevel;
		}

		private PendingIntent DeleteIntent(Android.Net.Uri uri)
		{
			// Create delete intent.
			Intent deleteOperation = new Intent (this, typeof(DeleteNotificationService));
			deleteOperation.SetData (uri);
			deleteOperation.PutExtra ("battery", GetBatteryLevel ());
			return PendingIntent.GetService (this, 0,
				deleteOperation, PendingIntentFlags.UpdateCurrent);
		}

		private Bitmap GetLargeIcon(DataMap pillDataMap) //string name)
		{
			// Get data from the DataMap
			var pillName = pillDataMap.GetString ("name");
			if (!pillImages.ContainsKey(pillName)) {
				var pillPic = pillDataMap.GetByteArray("pic");
				// Decode byte[] as bitmap.
				pillImages.Add(pillName, BitmapFactory.DecodeByteArray(pillPic, 0, pillPic.Length));
			}
			Bitmap pic;
			pillImages.TryGetValue (pillName, out pic);
			return pic;
			
			/*switch (name) {
			case "Simvastatin":
				return BitmapFactory.DecodeResource (Resources, Resource.Drawable.Simvastatin);
			case "Aspirin":
			case "Aspirin (x2)":
				return BitmapFactory.DecodeResource (Resources, Resource.Drawable.Aspirin);
			case "Nifedipine":
				return BitmapFactory.DecodeResource (Resources, Resource.Drawable.Nifedipine);
			case "Donepezil":
				return BitmapFactory.DecodeResource (Resources, Resource.Drawable.Donepezil);
			case "Atorvastatin":
				return BitmapFactory.DecodeResource (Resources, Resource.Drawable.Atorvastatin);
			}
			return null;*/
		}
	}
}