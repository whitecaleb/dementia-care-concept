namespace DementiaWearApp.Dependencies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Net;

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;

    using System.Net.Http;
    using System.Net.Http.Headers;

    using RestSharp;
    using DementiaApp.Domain;
    using Java.Net;
    using Javax.Crypto.Spec;
    using Javax.Crypto;
    using Android.Util;
    
    public class MedicationConfirmationProvider : Interfaces.IMedicationConfirmationProvider
    {
        public const string HubName = "dimentia-care-concepthub";
        public const string Endpoint = "https://dimentia-care-concepthub-ns.servicebus.windows.net/";
        public const string SharedAccessKeyName = "DefaultFullSharedAccessSignature";
        public const string SharedAccessKey = "kCvUiJLeB9JK717+IzLLoW43/pMtYaHTJpo9bOpAwPI=";

        public bool ConfirmMedicationHasBeenTaken(MedicationConfirmation confirmation)
        {
            // TODO: https://azure.microsoft.com/en-us/documentation/articles/notification-hubs-android-get-started/#how-to-send-notifications
            var client = new RestClient(Endpoint + HubName);
            var request = new RestRequest("/messages/?api-version=2015-01", Method.POST);

            request.AddHeader("Authorization", GenerateSASToken());
            request.AddHeader("Content-Type", "application/json;charset=utf-8");

            request.AddHeader("ServiceBusNotification-Format", "gcm");
            request.AddObject(confirmation);

            var response = client.Execute(request);

            return response.StatusCode == HttpStatusCode.OK;
        }

        /**
         * Example code from http://msdn.microsoft.com/library/azure/dn495627.aspx to
         * construct a SaS token from the access key to authenticate a request.
         * 
         * @param uri The un-encoded resource URI string for this operation. The resource
         *            URI is the full URI of the Service Bus resource to which access is
         *            claimed. For example, 
         *            "http://<namespace>.servicebus.windows.net/<hubName>" 
         */
        private string GenerateSASToken()
        {
            string targetUri = URLEncoder.Encode("https://dimentia-care-concepthub-ns.servicebus.windows.net/dimentia-care-concepthub", "UTF-8").ToLower();

            long expiresOnDate = DateTime.Now.Millisecond;
            int expiresInMins = 60; // 1 hour
            expiresOnDate += expiresInMins * 60 * 1000;
            long expires = expiresOnDate / 1000;
            string toSign = targetUri + "\n" + expires;

            // Get an hmac_sha1 key from the raw key bytes
            byte[] keyBytes = Encoding.UTF8.GetBytes(SharedAccessKey);
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA256");

            // Get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.GetInstance("HmacSHA256");
            mac.Init(signingKey);

            // Compute the hmac on input data bytes
            byte[] rawHmac = mac.DoFinal(Encoding.UTF8.GetBytes(toSign));

            // Using android.util.Base64 for Android Studio instead of 
            // Apache commons codec.
            string signature = URLEncoder.Encode(
                    Base64.EncodeToString(rawHmac, Base64.NoWrap).ToString(), "UTF-8");

            // construct authorization string
            String token = "SharedAccessSignature sr=" + targetUri + "&sig="
                    + signature + "&se=" + expires + "&skn=" + SharedAccessKeyName;
            return token;
        }
    }
}