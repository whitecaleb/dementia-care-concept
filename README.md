#Introduction
Read more about this concept project at our blog [http://www.thetechnologystudio.co.uk/blog/a-summer-of-android-wear/](http://www.thetechnologystudio.co.uk/blog/a-summer-of-android-wear/). All code in this concept app is provided to help people learn about Android Wear and is copyright Earthware Ltd. Permission must be gained before re-using it in any way other than for learning.

#Getting Started

Download xamarin studio and point it at an empty directory for your android SDK then xamarin will download and install everything you need including the android SDK. Once setup open the Android sdk manager (xamarin studio|Tools menu) and select the Android SDK level 21 also selecting the intel x86 packages for the emulator (see below)

# Developing with emulator

Its easiest to run the patient app on a real phone and emulate the watch app, deploy the app to the phone first (right click patientapp in xamarin studio and select run with then select the real phone). Once that's installed set the wearable app as startup project and debug it on the watch emulator.

Check before starting that the android wear app on the phone says the emulator is connected, see hints and tips below if not.

#Hints and tips

* make sure you install the intel x86 HAXM drivers (they are in android sdk manager in xamarin studio|Tools menu) to speed up the emulator
* sometimes the emulator doesn't connect to the android wear app (says waiting to connect or similar) open an SDK command prompt (android studio Tool menu) and type "adb -d forward tcp:5601 tcp:5601"